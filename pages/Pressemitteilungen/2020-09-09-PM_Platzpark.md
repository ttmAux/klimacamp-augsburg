---
layout: page
title:  "09.09.2020: Platzpark statt Parkplatz: Klimaaktivist*innen platzieren Hochbeet auf Maxstraßenparkplatz"
date:   2020-09-09 12:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 15
---

*Pressemitteilung vom Augsburger Klimacamp am 9. September 2020*

# Platzpark statt Parkplatz: Klimaaktivist\*innen platzieren Hochbeet auf Maxstraßenparkplatz

[https://www.speicherleck.de/iblech/stuff/.platzpark/bildergalerie](Fotos)

Aktivist\*innen des Augsburger Klimacamps belegen seit heute Früh einen
Parkplatz in der Maxstraße mit einem Hochbeet. Im Hochbeet sollen Blumen
gepflanzt werden, die einerseits schön anzusehen sind und andererseits dem
Bienen- und Insektensterben entgegenwirken. Zudem sind drei Fahrradstellplätze
integriert.

„Wir sehen unseren Platzpark als Modellversuch, Bodenversiegelung
zurückzudrehen und die Verteilung öffentlichen Raums neu zu denken“, erklärt
Sarah Bauer (16) die Aktion. „Obwohl sich Augsburg den Titel 'Fahrradstadt
2020' gab, fehlen überall Fahrradstellplätze. Oder wie oft mussten Sie Ihr
Fahrrad an Straßenlaternen oder gar an Geschäfte lehnen?“, fährt Bauer fort.
Der Platzpark schafft im Sinne einer echten Fahrradstadt drei
Fahrradabstellplätze.

Die Maxstraße bot sich aus mehreren Gründen an: So profitieren auch die
anliegenden Cafés von den schönen Blumen und dem damit einhergehenden
Wohlfühlcharakter. Zudem soll die Maxstraße nach Wunsch der
Bürger\*innenversammlung vom 11. Dezember 2019 autofrei werden. „Dann wird in
der Maxstraße noch mehr Raum für solche Platzparks sein.“

„Wir sehen unseren Platzpark als kleinen Beitrag und Denkanstoß zum Projekt von
unserer Oberbürgermeisterin Eva Weber, Augsburg zur klimafreundlichsten
Metropole Bayerns zu machen“, erklärt Bauers Mitstreiter Leon Ueberall (17).
„Wir fordern eine Mobilitätspolitik, die durch Ausbau und Vergünstigung von
Bussen und Trams sowie durch ein durchgängiges und sicheres Radwegenetz
langfristig ermöglicht, dass Augsburger\*innen kein teures Auto mehr benötigen.
Unorte wie die jetzige Karlstraße soll es nicht mehr geben. In Vorreiterstädten
wie Wien und Kopenhagen wurden und werden jedes Jahr 3 % der
Innenstadtautoparkplätze umgewidmet. Für Augsburg wünschen wir uns das auch.“

Ueberall verweist auch auf die „autozentrierte Stellplatzsatzung“, die nur in
engen Grenzen Eigentümer\*innen von Mehrfamilienhäusern erlaubt,
Fahrradabstellplätze bei ihren Häusern zu schaffen. „Stattdessen wird
Eigentümer\*innen verpflichtend vorgeschrieben, Autoparkplätze einzurichten.“

Neben der Mobilität geht es beim Platzpark auch um Artenvielfalt: 1,7 Millionen
Menschen in Bayern unterschrieben beim Volksbegehren 'Rettet die Bienen' und
sandten damit ein klares Signal an die Regierung. In Augsburg sei davon nichts
angekommen, so Bauer: „Entspannende Grünflächen gibt es in der Innenstadt kaum,
die wenigen Grünstreifen an Straßen sind nicht mit für Insekten tauglichen
Blumen bepflanzt. Es fehlen klare Balance-Akte – wie etwa Pflanzen,
Grünflächen, autofreie Innenstadtkonzepte – die das Stadtklima und den
natürlichen Lebensraum wiederherstellen und schützen.“


## Hintergrund

In Augsburg sind fast alle Flächen in Wohngebieten und Altstadt komplett
versiegelt. Beton und Asphalt speichern einen Großteil der Sonnenenergie, und
so wird es im der Stadt wesentlich heißer als im Umland. Dieser sogenannte
Hitzeinsel-Effekt verstärkt noch die große Belastung der allgemeinen
Klimaerwärmung. Das führt zu immer mehr Tropennächten pro Jahr und Tagen mit
einer Überschreitung der verträglichen bioklimatischen Belastung.

Die schon festgeschriebenen Klimaziele der Stadt Augsburg sowie die im
Koalitionsvertrag vorgeschlagenen Ergänzungen sehen vor, dass Augsburg bis 2050
noch 34 Millionen Tonnen CO₂ emittiert. Das ist drei Mal so viel, wie Augsburg
nach dem Pariser Klimaabkommen zusteht, wenn die Erderhitzung mit großer
Wahrscheinlichkeit auf 1,5 °C beschränkt werden soll.

In Augsburg kostet ein Bewohnerparkausweis im Jahr gerade mal 30 €. Eine
Jahreskarte für den ÖPNV dagegen 583 €.

In Augsburg kommt es jedes Jahr zu etwa 1800 Verkehrsunfällen mit
Personenschaden. Dabei starben 2018 sieben Menschen im Straßenverkehr, fünf
waren auf dem Fahrrad unterwegs. Personenkraftwagen sind dabei mit Abstand
immer noch die Unfallverursacher Nummer eins.
