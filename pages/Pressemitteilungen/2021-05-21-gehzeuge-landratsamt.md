---
layout: page
title:  "21.5.2021: Mit dem Gehzeug zum Landratsamt: Klimagerechtigkeitsaktivist*innen fordern Mobilitätswende im Großraum Augsburg"
date:   2021-05-21 02:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 46
---

*Pressemitteilung vom Klimacamp Augsburg am 21.05.2021*

# Mit dem Gehzeug zum Landratsamt: Klimagerechtigkeitsaktivist\*innen fordern Mobilitätswende im Großraum Augsburg

Am kommenden Dienstagnachmittag (25.05.2021) machen Klimagerechtigkeitsaktivist\*innen mit einer Gehzeugaktion auf die am selben Tag stattfindende Sitzung im Landratsamt Augsburg aufmerksam. Dort werden sich die Gemeinden aus der Nähe von Augsburg treffen, um kommunale Forderungen zum Mobilitätskonzept des Landkreises einzureichen. "In dieser Sitzung werden die Weichen für den zukünftigen Verkehr in und um Augsburg gestellt. Und es wird sich zeigen, ob im Landkreis Augsburg die dringend notwendige Mobilitätswende angeht, oder im letzten Jahrhundert zurückbleibt.", so die Einschätzung von Klimacamperin Ute Grathwohl (48 Jahre). Mit selbstgebauten Gehzeugen – Holzgestellen in der Größe eines PKWs – wird eine Route von der Blücherstraße über Hochfeld bis zum Landratsamt in Schnellbus-Formation abgelaufen. Ein Gehzeug karikiert dabei den enormen Platzbedarf des motorisierten Individualverkehrs.

Die gewählte Strecke ist entnommen aus dem Verkehrskonzept "Verkehr 4.0 für den Ballungsraum Augsburg" [1]. Laut Konzept sollen Schnellbusse auf dieser und weiteren Strecken fahren, um Augsburgs Straßenbahnlinien ringförmig miteinander zu verbinden. Dadurch sollen Lücken im ÖPNV geschlossen und  Verbindungen massiv verkürzt werden. Außerdem werden Stadtteile miteinander verbunden, die im bislang sternförmigen ÖPNV-Netz erst einen Umweg ins Stadtzentrum nehmen müssen.

Mit dieser Aktionsform soll eine Abwendung von weiterer priorisierter Förderung des Autoverkehrs und ein massiver Ausbau des öffentlichen Personennahverkehrs gefordert werden. "Der Individualverkehr hat keine Zukunft, da gerade in Städten der hohe Platzbedarf und die Belastung durch Lärm und Schadstoffe bereits jetzt zu erheblichen Problemen führen. Zudem wird die Klimakrise durch den bedeutend höheren CO2-Ausstoß pro Personenkilometer verschärft und das Einhalten des 1,5°C-Limits rückt in weite Ferne.", erklärt Schüler Laurenz Werner (17 Jahre). Daher schlagen die Aktivist\*innen unter Anderem eine Umsetzung des Konzepts Verkehr 4.0  in der Region und eine deutliche Vergünstigung und Vereinfachung der Ticketpreise vor.

"Der Landkreis und die Stadt haben jetzt die Gelegenheit, alles in die Wege zu leiten, um die Mobilität in und um Augsburg zukunftsfähig zu machen. Ein großer Teil der nötigen Infrastruktur ist mit dem aktuellen Straßennetz bereits geschaffen und muss nur sinnvoll genutzt werden. Unter anderem durch den hohen Verschleiß an Straßen durch schwere Fahrzeuge, gesundheitliche und sozioökonomische Folgen, zahlt die Stadt außerdem bei jedem gefahrenen Autokilometer drauf. Am Geld sollte es also nicht scheitern.", so angehender Förster Nico Kleitsch (21).

[1] https://www.verkehr4x0.de/konzept-verkehr4x0/

## Hinweis
Die als Demonstration angemeldete Aktion wird von 14 bis etwa 16 Uhr stattfinden. Pressevertreter\*innen sind bei Interesse herzlich eingeladen, die Aktivist\*innen zu begleiten. Aktuelle Informationen über den Verlauf gibt Nico Kleitsch(0176 41975343).

## Über die Organisator\*innen
Die Aktion wird von Aktivist\*innen aus dem Augsburger Klimacamp durchgeführt.

## Kontakt
Nico Kleitsch(0176 41975343)