---
layout: page
title:  "10.11.2020: Gerichtsurteil: Klimacamp darf bleiben"
date:   2020-11-10 16:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 25
---

*Pressemitteilung vom Augsburger Klimacamp am 10. November 2020*

# Gerichtsurteil: Klimacamp darf bleiben

Das Bayerische Verwaltungsgericht Augsburg entschied (Au 8 K 20.1179) [1], dass
das Klimacamp bleiben dürfe. Seit dem 1. Juli weisen dort Tag und Nacht
Aktivist\*innen darauf hin, dass die Stadt Augsburg entgegen ihres öffentlichen
Images Klimagerechtigkeit nicht ernst nimmt und plant, das Augsburg zustehende
Treibhausgasrestbudget um das Dreifache zu überschreiten. Die Stadt begegnete
diesem friedlichen Protest junger Menschen mit einem Räumungsbescheid, den das
Gericht nun für unrechtmäßig erklärte.

Das Gerichtsverfahren lief seit Tag 9 des Camps (9. Juli 2020) und dauerte so
lange, da die Stadt immer wieder um Fristverlängerung bat. Damals erklärte
Oberbürgermeisterin Eva Weber, dass sie das Klimacamp nicht mehr als politische
Versammlung sehe, und bat die Klimacamper\*innen, die Kundgebung bis 18 Uhr des
Folgetages zu beenden, da sie „wahrscheinlich vor Gericht sowieso keine Chance
hätten“.

"Wir hoffen immer noch, das Klimacamp so schnell wie möglich aufgeben zu
können", erklärt Janika Pondorf (16), Camperin der ersten Stunde. "In den 133
Tagen Klimacamp ist in Sachen Klimagerechtigkeit allerdings nichts passiert.
Die Stadtregierung ist für uns in ihren Beteuerungen, Klimaschutz ernst zu
nehmen und das Engagement junger Menschen zu schätzen, nicht authentisch.
Alleine schon, weil sie sehenden Auges seit Klimacamperöffnung bereits 7 % des
Augsburg zustehenden CO₂-Restbudgets verbrauchte. Die Stadt muss dringend
einlenken!"

Dass die Stadt das Thema nicht verstehe, erkenne man auch in ihren
Begründungsversuchen im Rahmen des Gerichtsprozesses. "Die Stadt attestierte
etwa unserer Unterschriftensammlung für mehr und sicherere Radwege nur 'im
weitesten Sinne' einen Bezug zu unserem Versammlungsthema Klimagerechtigkeit."
Dabei ist der motorisierte Verkehr Deutschlands drittgrößte Quelle an
Treibhausgasemissionen, und Augsburg zu einer echten Fahrradstadt zu machen,
liegt vollständig im Wirkungskreis der Stadt. "Auch unseren Veranstaltungen zu
Feminismus und direkter Demokratie sprach die Stadt Klimagerechtigkeitsbezug
ab." Dabei zeigen Bürger\*innenversammlungen immer wieder, welch effiziente
Klimagerechtigkeitsmaßnahmen der Querschnitt der Bevölkerung vorschlägt
(zuletzt in Frankreich [2]: Tempolimit 110 km/h, Verbot von Inlandsflügen und
Fleischgerichten in öffentlichen Kantinen). Frauen sind im weltweiten
Durchschnitt stärker von der Erdaufheizung betroffen, tragen weniger zu ihr bei
und sind seltener in relevanten Entscheidungspositionen.

"Wir hoffen, dass die Stadt mit uns nun ein inhaltliches Gespräch sucht. Ein
solches gab es seit Beginn des Camps nicht", ergänzt Pondorfs Mitstreiterin
Stefanie Bauer (18). "Zudem hoffen wir, dass die Stadt ihre Finanzmittel statt
in einen Berufungsprozess lieber in Klimagerechtigkeitsmaßnahmen investiert."
Die Klimacamper\*innen geben zu bedenken, dass sie keineswegs radikale
Forderungen hätten, sondern lediglich auf die Einhaltung des demokratisch
beschlossenen Pariser Klimaabkommens drängen und der Wissenschaft Gehör
verschaffen. Zudem gebe es mehrere Maßnahmen, die die Stadt Null Euro koste --
etwa veröffentlichte das Bündnis "Fahrradstadt jetzt" eine Null-Euro-Liste an
Maßnahmen. Auch ein Appell an die Bundesregierung, auf
Wirtschaftswissenschaftler\*innen zu hören und eine wirksame und gerechte
CO₂-Bepreisung einzuführen, koste nichts. "Wenn die Regierungen aber weiterhin
unsere Zukunft akut bedrohen, müssen und werden wir zu friedlichen Aktionen des
zivilen Ungehorsams übergehen."

[1] https://www.vgh.bayern.de/media/vgaugsburg/presse/pm_2020-11-10_klimacamp_november2020.pdf
[2] https://www.zeit.de/politik/ausland/2020-06/klimapolitik-frankreich-buergerrat-klimaschutz-gelbwesten-direkte-demokratie
[3] https://augsburg.klimacamp.eu/pages/Pressemitteilungen/2020-10-10-PM_100.html
[4] https://augsburg.klimacamp.eu/pages/Pressemitteilungen/2020-08-03-PM_Weber.html

## Hinweis

Den gesamten Schriftverkehr mit der Stadt geben wir gerne auf Anfrage
heraus. Eine formlose Mail an alex.mai@posteo.net genügt. Über den Inhalt der
Dokumente darf berichtet werden, nur dürfen die Dokumente selbst nicht
veröffentlicht werden.

## Anmerkung

Anders als in den USA, in denen die Klimakrise von der Regierung
aktiv geleugnet wird, beteuern deutsche Politiker\*innen immer, Klimaschutz
ernst zu nehmen. Ein Blick auf die Beschlusslage zeigt aber, dass sie dies
nicht tun. Seit Eröffnung des Klimacamps passierte folgendes:

* Auf Bundesebene wurde das Kohleeinstiegsgesetz beschlossen, dank dem unsere
  Steuergelder noch bis 2038 zur Subvention von Kohlekraftwerken verwendet
  werden sollen. Die damit ausgestoßene CO₂-Menge ist so groß, dass Deutschland
  sein Treibhausgasbudget für die 1,5-Grad-Marke erheblich überschreiten wird.

* Auf Landesebene wurde ein Klimagesetz diskutiert, das von allen
  Expert\*innen (bis auf die beiden, die von CSU und AfD bestellt wurden) als
  unzulänglich kritisiert wurde: nicht 1,5-Grad-kompatibel, keine
  Budgetangaben, keine Zwischenkontrollen, keine Verbindlichkeit, ...

* In Augsburgs Stadtrat brachte die Opposition mehrere gute Anträge ein:
  Radentscheid, dezentrale Energiewende, Klimanotstand, Analyse der
  Forderungen des Klimacamps, ... Die wurden allesamt nicht zur Diskussion
  zugelassen, sondern auf unbestimmte Zukunft vertagt. Augsburg ist in
  Sachen Klimagerechtigkeit genauso weit wie vor 133 Tagen. Einzige
  Ausnahme: Eine Straße wurde zur Fahrradstraße.

Und trotz dieser gefährlichen Fehlentwicklung erklärte CSU-Stadtrat Max Weinkamm 
bei unserer letzten Podiumsdiskussion [3], er verstehe die Verzweiflung der
Schüler nicht. Frau Weber selbst verwies in dem letzten Interview, das wir von
ihr kennen [4], auf die Leistungen, die bisher schon erbracht wurden, und
darauf, dass Augsburg Bayerns größter kommunaler Waldbesitzer sei.
