---
layout: page
title:  "06.08.2020: Aktivist*innen des Augsburger Klimacamps wandeln eine Fahrspur in einen Radweg"
date:   2020-08-06 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 7
---

*Pressemitteilung des Augsburger Klimacamps am 6. August 2020*

# Aktivist\*innen des Augsburger Klimacamps wandeln eine Fahrspur in einen Radweg

Eine Pop-up-Bike-Lane taucht plötzlich auf und vermittelt kurzzeitig ein Bild davon, wie Rad fahrende Menschen unterwegs sein könnten, wenn Politik und Verwaltung ihnen den Platz einräumen würden. Sie ist ein Kurzzeit-Experiment: Sie zeigt kurzzeitig und jenseits aller Planfeststellungsverfahren, wie eine fahrradfreundliche Infrastruktur aussehen und welchen Einfluss sie auf den Verkehr ausüben kann. Zumindest kurzzeitig wird ein geschützter Radfahrstreifen Realität.

Die Aktivist\*innen des Klimacamps werden eine solche temporäre Radspur am morgigen Freitag (7. August 2020) von 8:00 Uhr bis 8:30 Uhr auf der Hermanstraße errichten (stadteinwärts).

„Die Hermanstraße ist für Fahrradfahrer\*innen ein bekannter Unfallschwerpunkt. Dem Änderungsplan von CSU-Baureferent Gerd Merkle fehlt es an Mut, er ist ein fauler Kompromiss und wie für die Radwegeplanung in Augsburg leider üblich nur Stückwerk“, erklärt Luzia Menacher (19) vom Augsburger Klimacamp.

„Dem Änderungsplan fehlt Mut, da er den Autoverkehr nicht einschränkt. Der Plan ist deswegen ein fauler Kompromiss, da er vorsieht, dass sich die Straßenbahn den Platz mit der Autofahrspur teilt. Und er ist Stückwerk, da ein barrierefreier Umbau der Straßenbahnhaltestelle unterbleibt.“

Die Aktivist\*innen unterbreiten auch konstruktive Vorschläge: „Aus einem Guss wäre stattdessen ein integriertes Verkehrskonzept, dafür keine Parkplätze in der Hermanstraße. Und wirklich mutig wäre es, den Autoverkehr stadteinwärts zu verbieten. Autos könnten nach einer Schleife Stettenstraße--Localbahnstraße durch die Ladehöfe ausweichen. Damit wäre genug Platz für eine Lösung ohne faulen Kompromiss.“

Das Klimacamp kritisiert die Ambitionslücke der Stadt in Bezug auf die Hermanstraße: 2020 Vorplanungen, 2021 Planung, 2022 Umsetzung. Ein glaubwürdig und angemessen umgesetztes Projekt 'Fahrradstadt' hätte zumindest eine Achse vom Stadtrand bis zur Stadtmitte vollständig so umgesetzt. Damit wurde nach einer ganzen Legislaturperiode Augsburgs Bürger\*innen die Möglichkeit genommen, an einem guten Beispiel zu sehen, wie eine Fahrradachse aus ihrem Stadtteil aussehen würde. Das dringende Bedürfnis nach einer lebenswerten Stadt, die Klimaziele erreicht, wartet so auf den Sankt-Nimmerleins-Tag. „Wir alle haben uns viel zu sehr daran gewöhnt, dass es Unorte in Augsburg gibt, an denen wir uns wirklich ungern aufhalten. Das muss aber nicht sein! Wir Menschen können uns die Stadt zurückholen!“, erklärt Menacher ihre Motivation, seit 36 Tagen auf den Komfort ihrer Wohnung zu verzichten und im Klimacamp zu leben.
