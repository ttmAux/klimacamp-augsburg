---
layout: page
title:  "31.5.2021: Aktivist*innen strahlen mit Laserbeamer Schlackeberge der Lechstahlwerke in Meitingen an"
date:   2021-05-31 03:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 47
---

*Pressemitteilung von Wald statt Stahl am 31. Mai 2021*

# Aktivist\*innen strahlen mit Laserbeamer Schlackeberge der Lechstahlwerke in Meitingen an

Sperrfrist Montag (31.5.2021) 22:30 Uhr
{: .label .label-red }

**[ Diese Pressemitteilung gab nicht das Klimacamp, sondern Wald statt Stahl
heraus. Sie findet sich auf dieser Seite wegen der thematischen Relevanz der
Bequemlichkeit halber trotzdem. ]**

Am Abend des 31. Mai projizierten Klimagerechtigkeitsaktivist\*innen des
Aktionsbündnisses „Wald statt Stahl“ mit einem Laserbeamer
den Spruch „Lohwi bleibt“ auf einen der Schlackeberge der Lechstahlwerke in
Meitingen. Damit prangerten sie die bevorstehende Rodung des anliegenden Lohwalds
an und wollten Druck auf die Lokalpolitik und die Stahlwerksbetreiber aufbauen.

Ebendieser Wald soll nämlich für den Ausbau des Werks gerodet werden. Der Wald
ist ein Bannwald und damit durch das bayerische Waldgesetz geschützt, einem
Gutachten des BUNDs zufolge ist er ökologisch sogar besonders schützenswert.
Max Aicher, Besitzer der Lechstahlwerke und einer der reichsten Menschen
Bayerns, versucht schon seit mehreren Jahren, die Waldfläche für einen
Ausbau des Stahlwerks umzuwidmen. „Aicher ist ein skrupelloser Finanzhai, der
seine Vermögensvermehrung ohne Rücksicht auf die Lebensqualität der ansässigen
Bewohner\*innen und den voranschreitenden Klimawandel durchsetzen möchte“, so
der an der Aktion beteiligte Marvin Knautsch (19).

Gerade in Anbetracht ihrer Funktion als Kohlenstoffsenke stellen lebendige
Wälder einen wichtigen Stützpfeiler im Kampf gegen den Klimawandel dar. Bei
einem Ausbau des Stahlwerks anstelle des Waldes würde nicht nicht nur diese
Funktion zugrunde gehen, sondern durch die CO2-intensive Stahlherstellung sogar
noch ein intensivierter Kohlenstoffausstoß anstelle dessen stattfinden. „Das
ist nicht nur aus wissenschaftlicher Sicht sehr bedenklich, sondern auch in
einer sozialen Dimension. Während Menschen im globalen Süden schon jetzt unter
den Folgen des Klimawandels leiden, ohne je nennenswerte CO2-Emissionen
verursacht zu haben, können reiche, alte weiße Männer, die verantwortlich für
den Großteil schädlicher Treibhausgasemissionen sind, ungebremst in ihrem
rücksichtslosen und menschen- und planetenverachtenden Treiben fortfahren. Eine
große Mitschuld daran trägt das Wirtschaftssystem, das Großkapitalisten wie
Aicher in die Hände spielt. So wie wir den Kapitalismus überwinden müssen, muss
auch Einzelpersonen wie Aicher Einhalt geboten werden, darum sind wir hier und
werden erst wieder gehen, wenn das Fortbestehen des Waldes gesichert ist!“, so
Knautsch weiter.

Wie bereits angekündigt werden die Aktivist\*innen den Wald demnach, angelehnt an
vorangegangene Waldbesetzungen, besetzen. „Die Räumungskosten bei einem solchen
Unterfangen sind erfahrungsgemäß so hoch, dass sich die Gemeinde Meitingen mehr
als zweimal überlegen wird, ob der Stahlwerksausbau genehmigt werden sollte“,
sagt Klimagerechtigkeitsaktivistin Lucia Reng (21).

## Fotos zur freien Verwendung

Hier herunterladen: https://www.speicherleck.de/iblech/stuff/.lw6 (wird im
Laufe des Abends gefüllt) Falls benötigt, Quellenangabe "Wald statt Stahl". Von
unserer Seite aus ist keine Quellenangabe nötig.

## Hinweis

Die Aktion wird am Montag den 31.5. gegen 21 Uhr bei den Schlackebergen der
Lechstahlwerke stattfinden und bei einbrechender Dunkelheit eindrucksvolle
Lichtspiele erzeugen. Aufgrund möglicher polizeilicher Intervention müssen wir
eventuell spontan umdisponieren. Aktuelle Informationen über den Verlauf der
Aktion gibt Ingo Blechschmidt (+49 176 95110311).

## Über das Aktionsbündnis

"Wald statt Stahl" ist ein Zusammenschluss von
Klimagerechtigkeitsaktivist\*innen diverser Bewegungen aus Meitingen, Augsburg
und weiteren Städten aus der Region. Sie eint das Ziel, mit Aktionen und
zivilem Ungehorsam den Meitinger Lohwald zu erhalten und seine Erweiterung
einzufordern. Am 9.4.2021 fand die erste Aktion des Aktionsbündnisses statt,
weitere werden folgen. "Wald statt Stahl" agiert unabhängig vom
Bannwald-Bündnis Unterer Lech, welches sich ebenfalls für den Erhalt des
Lohwalds einsetzt.
