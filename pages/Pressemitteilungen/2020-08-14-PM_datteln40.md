---
layout: page
title:  "14.08.2020: Protest gegen das neue Kohlekraftwerk Datteln 4"
date:   2020-08-14 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 10
---

*Pressemitteilung vom Augsburger Klimacamp am 14. August 2020*

# Protest gegen das neue Kohlekraftwerk Datteln 4

Trotz ungesicherter Rechtslage und jahrelangen Protesten von Anwohner\*innen und internationalen Aktivist\*innen nahm Uniper am 30. Mai 2020 in Datteln ein neues Kohlekraftwerk ans Netz. Seit diesem Tag gibt es zahlreiche Demonstrationen und friedliche Aktionen zivilen Ungehorsams vor Ort. Eine Übersicht über die Problematik gibt der BUND Naturschutz: https://www.bund-nrw.de/themen/klima-energie/hintergruende-und-publikationen/steinkohlenkraftwerke/uniper-kohlekraftwerk-datteln-iv/

Am heutigen 14. August rufen zahlreiche Eltern, die sich bundesweit und international zu den „Parents for Future“ zusammen fanden, zu dezentralen Aktionen gegen Datteln 4 und für eine der Klimakrise angemessene Klimapolitik auf.

\*\*Das Augsburger Klimacamp solidarisiert sich mit den Aktivist\*innen in Datteln und anderswo, die sich heute auf dieser Ebene für eine gedeihliche Zukunft einsetzen.\*\* Um 13:00 Uhr werden wir mit einem Foto unserer Solidarität Ausdruck verleihen und stehen für Fragen und Einschätzungen zur Situation zur Verfügung. Ab 15:00 Uhr verfolgen wir den Protest vor Ort über einen Live-Stream, zu dem ebenfalls alle Interessierten eingeladen sind.

Für weitere Details verweisen wir auf die Pressemitteilung der engagierten
Eltern: https://parentsforfuture.de/de/node/2662
