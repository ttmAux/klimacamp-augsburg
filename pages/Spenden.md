---
layout: page
title: Spenden
permalink: /spenden/
nav_order: 130
---

# Spenden
## Geldspenden
Wir sind über Spenden sehr dankbar und wissen diese sehr zu schätzen.

Wir finanzieren darüber Campmaterialien, Flyer, Plakate, Banner, gelegentlich
Klettermaterialien, juristische Ausgaben und diverse kleinere Posten.

Dankenswerterweise nimmt die [Bürgerstiftung
Augsburg](https://www.buergerstiftung-augsburg.de/) für uns Spenden entgegen.
Die Kontodaten lauten:

    Name: Bürgerstiftung Augsburg
    IBAN: DE22 7205 0000 0000 0263 69
    Verwendungszweck: Klimacamp (unbedingt angeben, sonst kann die Spende nicht zugeordnet werden!)

## Sachspenden
Es gibt es ein paar Dinge, die wir immer wieder gebraucht werden und die gerne jederzeit [im Camp](/mitmachen/#adresse) vorbei gebracht werden können.

_Aber_: Teil dieser Bewegung ist auch Kapitalismuskritik. Es ist nicht im Sinne von einigen der Aktivisti, wenn Leute Dinge, die auf der Liste stehen, einkaufen, um den Bedarf schnellst- und bestmöglich zu decken, vielmehr geht es Leuten hier darum, sich solidarisch zu organisieren, Materialien zu teilen, bereits vorhandene Dinge zur Verfügung zu stellen oder auch „nur“ den eigenen Überfluss an Leute weitergeben, die ihn benötigen.

### Dringend benötigt
- Edings und andere Permanentmarker
- Mannschaftszelte , feuerfeste Mannschaftszelte, Bundesehzelte, Küchenzelte
- Fahrradschlösser, Fahrräder, Lastenrad
- Beamer (gerne auch als Leihgabe)

### Werkzeuge und Baumaterial
- Säge, Hammer, Zangen, Schraubenzieher, Schraubschlüssel, ...
- Akkuschrauber, Akkubetriebenes Werkzeug, Akkus
- Holzbretter/-latten und Kanthölzer (so lang wie möglich)
- Paletten
- Planen (LKW-Planen, Werbebanner, etc.)
- Wellblech und ähnliche Dächer
- bruchfeste transparente (Plexiglas, Polycarbnat) Scheiben
- Panzertape
- Brechstange, Axt
- Handkarren, Schubkarren,
- Metallstangen
- Schrauben, Nägel, Metallwinkel, ...
- Fenster

### Küchenutensilien
- bruchfestes, wiederverwendbares Geschirr (besonders Becher und  Teller) aus Emaille oder Hartplastik
- Große Töpfe
- sonstiges Geschirr (bruchfest)
- Besteck
- Großküchenbedarf
- **Nudelmaschine**

### Essen (vegan, im Besten Fall Bio und Regional)
- **vegane Aufstriche**
- vegane Milch
- Getränke in Mehrwegverpackungen
- **Snacks, vegane Schokolade und Müsliriegel**
- Grundnahrungsmittel (Nudeln, Reis, Linsen, ...)
- Konserven (passierte Tomaten, Bohnen, Kichererbsen)
- sonstiges lang haltbares Essen

### Fahrradbedarf
- **Schlösser**
- Fahräder, Lastenräder & Anhänger
- Schläuche
- Werkzeug (Inbusschlüsssel, Schraubenschlüssel, ...)
- Pumpe

### Elektronik
- **Powerbanks**
- Laptops
- Handys
- Registrierte SIM-Karten
- **Kopflampen** (am besten mit Rotlicht)
- **Megaphone**
- Solarpanel
- MTTP Solarwechselrichter

### Klettermaterialien
*Gebrauchtes Klettermaterial bitte immer beschriften mit Art, Kaufzeitpunkt, Nutzungsdauer und -intensität (Zettel dran, nicht direkt draufschreiben).*
- Karabiner jeder Art
- Seilrollen
- Drahtseile
- Gurte und Seile

### Outdoor 
- **Hängematten**
- Sturmfeuerzeuge (nachfüllbar)
- Feuerzeuge
- Taschenmesser, Rettungsmesser
- stabile Trinkwasserkanister alle Größen
- **Rucksäcke**
- verschließbare Taschen
- Zelte
- Isomatten, Luftmatratzen (auch mit Löchern)
- **Schlafsäcke** und Decken
- Sturmmasken

### Kleidung 
- Gummistiefel
- Thermo-Unterwäsche
- Tarnkleidung (in Oliv, grün, braun, schwarz)
- Regenkleidung, Winterkleidung
- Schlauchschale (in schwarz,oliv)
- Winterschuhe
- Wintersocken
- Arbeitshandschuhe

### Sonstiges
- Sekundenkleber
- Abtönfarbe und Acrylfarbe
- Bannerstoff
- Glitzer
- Erste-Hilfe-Sets
- Rasierklingen
- Klavier
- Desinfektionsmittelspender
- große sicher verschließbare Kisten
- leere Blöcke und Hefte
- **Eddings** und andere Stifte





