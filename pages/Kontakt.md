---
layout: page
title: Kontakt
permalink: /kontakt/
nav_order: 131
---

# Kontakt
Bei allen Anliegen (inkl. **Presseanfragen**) zum Klimacamp schreibt bitte an [klimacamp@systemli.org](mailto:klimacamp@systemli.org).

Direkter Kontakt zur rechtlich verantwortlichen Person: +49 176 95110311 (Ingo Blechschmidt, Arberstr. 5, 86179 Augsburg, iblech@web.de). 
Weiterer Kontakt: +49 177 3639551 (Paula Stoffels, paula.stoffels@web.de).


